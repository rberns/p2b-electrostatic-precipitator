# Theoretical analysis of ESP

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# https://www.apti-learn.net/lms/register/display_document.aspx?dID=266

# Particle-Migration Velocity, https://ppcair.com/pdf/EPA%20Lesson%20Lesson%203%20-%20ESP%20Parameters%20and%20Efficiency.pdf
# def 