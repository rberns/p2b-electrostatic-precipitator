# Metingen


### Metingen 20 april 2021

\#01 Van de Graaff generator in combination with thin metal wire electrode.

\#02 Van de Graaff generator in combination with thin metal wire with metal brush structure.

\#03 Van de Graaff generator in combination with thin metal wire electrode.

### Metingen 26 april 2021

\#01 Thick electrode, d = 1.99 \pm 0.01 mm
- Spanning: 0.5 cm gap -> 15 kV, meting: 20 kV+ (elektrometer maxed out)

\#02 Thin electrode, d = 0.105 \pm 0.01 mm
- Spanning: 7 \pm 0.3 kV met elektrometer, 3 mm met Van de Graaff generator

\#03 Brush electrode, 7 pieces copper brush structure -> radius  1 cm, zie labjournaal, d_{brushe wires} = 0.16 \pm 0.01 mm
- Spanning: 5 kV? Check met elektrometer

\#04 (half) 4 out of 7 brushes, 
- Spanning: 5 kV met elektrometer (constant)

### Metingen 11 mei 2021

\#01 Draad met diameter d = 0.5195 \pm 0.010 mm
- Spanning: 12.5 kV \pm 2 kV met elektrometer

### Note

We must assume that the voltage readings from the elektrometer have a certain degree of error. Inside the elektrometer corona discharge can take place. Therefore there might be a systemic failure in our readings.

## Identifier of experiment

20apr#01-1D045FB2-447B-4148-AACB-9B8B3FDF9D5A.pkl

DDMMM#NN-{voltage}

data\/(\d{2})(\w{3})\#(\d{2})(\w)v(\w)

### Types of electrode

| Identifier | Description |
| ---------- | ----------- |
| a | Thick electrode d = 1.99 \pm 0.01 mm|
| b | Thin electrode d = 0.105 \pm 0.01 mm|
| c | Brush electrode 7 brushes |
| d | Brush electrode 4/7 brushes |
| e | Medium electrode d = 0.5195 \pm 0.010 mm|

### Spanning Van de Graaff generator
-(184 \pm 7) * 10 V

1840 V plus minus 70 V

1000 \pm 10% MOhm -> 1.8 uA


