#%%
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import matplotlib.ticker as mtick
from matplotlib import colors
import json 
import os

palette_name = "mako"

sns.set_palette(palette_name)

cmap = sns.color_palette(palette_name)
cmaplight = sns.color_palette("mako", as_cmap=True)

prop_cycle = plt.rcParams['axes.prop_cycle']
colors_gen = prop_cycle.by_key()['color']

palette_file_out = open("colors.json", "w")
palette_file_out.write(json.dumps(colors_gen))
palette_file_out.close()

if __name__ == "__main__":  
    os.system("./ColorRGBAppleColorPalette colors.json")

color = [colors.rgb2hex(color) for color in colors_gen]
print(color)


# %%
