import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

dfRH = pd.read_csv('errorofDHT12humiditysensor.csv', header=None)
dfT = pd.read_csv('errorofDHT12tempsensor.csv', header=None)

humidityerrorfit = np.polyfit(dfRH[0].values, dfRH[1].values,
                   deg = 5)

temperrorfit = np.polyfit(dfT[0].values, dfT[1].values,
                   deg = 8)

def errorvshumidity(RH):
     return np.polyval(humidityerrorfit, RH)

def errorvstemp(T):
     return np.polyval(temperrorfit, T)

# RH = np.linspace(0,100,100)
#
# print(RH)
#
# plt.plot(dfRH[0].values, dfRH[1].values)
# plt.plot(RH, errorvshumidity(RH))
# plt.show()

# T = np.linspace(-30,70,100)
#
# print(T)
#
# plt.plot(dfT[0].values, dfT[1].values)
# plt.plot(T, errorvstemp(T))
# plt.show()
