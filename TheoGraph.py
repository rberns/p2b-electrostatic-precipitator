# %%
# Theoretical analysis

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import glob

import P2B
from practicum import *
import re

# %%

V = np.linspace(0, 20000, 100)
#c_values = np.linspace(0, 0.00000005, 10)

c_values = np.linspace(0, 0.0005, 10)

V_exp = [5000, 7000, 15000]
EF_exp = [0.69, .82, .86]



# %%
for c in c_values:
    EF = 1-np.exp(-c*(V))
    plt.plot(V, EF, label="c = {:.1g}".format(c))

plt.plot(V_exp, EF_exp)

plt.legend()
plt.show()

# Compare with Khaled, U., Beroual, A., Alotaibi, F., Khan, Y., & Al-Arainy, A. (2018). Experimental and analytical study on the performance of novel design of efficient twostage electrostatic precipitator. IET Science, Measurement and Technology, 12(4), 486–491. https://doi.org/10.1049/iet-smt.2017.0468

# %%

# %%
