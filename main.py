import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

from config import *
import P2B

def filter_by_time(interval, data):
    [start, end] = interval
    return data.loc[(data["Elapsed"] > start) & (data["Elapsed"] < end)]


def get_mean_std(data, column):
    return data[column].mean(), data[column].std()/np.sqrt(data[column].count())


def get_mean_std_in(interval, data, column):
    return get_mean_std(filter_by_time(interval, data), column)


def get_mean_std_in_by_file(interval, file_name, column):
    data = pd.read_pickle(file_name)

    data = data.loc[(data["PM25"] != -1.0) & (data["PM10"] != -1.0)]

    result = get_mean_std_in(interval, data, column)

    print(column, result)

    return result





# %%


i = 1


print("baseline PM25")
basepm25 = get_mean_std_in_by_file(files["baseline"][1], files["baseline"][0], "PM25")
print("baseline PM10")
basepm10 = get_mean_std_in_by_file(files["baseline"][1], files["baseline"][0], "PM10")
print("1 PM10")
Apm10 = get_mean_std_in_by_file(files["1"][1], files["1"][0], "PM10")
print("1 PM25")
Apm25 = get_mean_std_in_by_file(files["1"][1], files["1"][0], "PM25")

eff25 = P2B.fvf_delen2(Apm25, basepm25)
eff10 = P2B.fvf_delen2(Apm10, basepm10)

print("Efficiency:", eff25, eff10)

# data = pd.read_pickle("measurementvandegraafon#02-D5349803-B8A1-4CC9-B5D2-D94258AECC8A.pkl")
# data = pd.read_pickle("measurementvandegraafonpolchange#03-F2E18415-9768-4382-BAF8-FFAD6B373165.pkl")
# data = pd.read_pickle("measurementventibase#04-66D68838-9BE5-4A94-9B92-B5578186FA82.pkl")
# data = pd.read_pickle("measurementventi#05-103FE1DA-644E-4F9E-9723-0DA5F880FCC9.pkl")
# data = pd.read_pickle("measurementincensevent#08-08334058-D60F-4824-B2F1-4C2C3F535535.pkl")
# data = pd.read_pickle("measurementincensevent#08-7DDFF5D1-99BC-4487-9EA8-DB17FD0DAAFD.pkl")
# data = pd.read_pickle("measurement#11-0EE766FE-262D-44A7-85AD-90727626BEA3.pkl")
data = pd.read_pickle(files["1"][0])

data = data.loc[(data["PM25"] != -1.0) & (data["PM10"] != -1.0)]

#print("PM25", get_mean_std_in(files["baseline"][1], data, "PM25"))

#data = pd.read_pickle(files["1"][0])

#data = data.loc[(data["PM25"] != -1.0) & (data["PM10"] != -1.0)]

#print("PM25", get_mean_std_in(files["1"][1], data, "PM25"))

data.plot(x="Elapsed", y="PM25", title="FULL PM25");
data.plot(x="Elapsed", y="PM10", title="FULL PM10");

data = filter_by_time(files["baseline"][1], data)

# %%

print("PM25", data["PM25"].mean(), data["PM25"].std())
print("PM10", data["PM10"].mean(), data["PM10"].std())

data.plot(x="Elapsed", y="PM25");
data.plot(x="Elapsed", y="PM10");

plt.show()
