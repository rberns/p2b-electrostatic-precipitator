# %%

import numpy as np
import scipy.constants as constants
import scipy as sp
import practicum

P = 101325.0 #760/760 #101325.0 / 101325.0 * (748/760) # p/p0 (p0 is atmospheric pressure)

T = 293.0 # in Celsius

r0 = np.array([[0.105E-3 / 2.0, 0.5195E-3 / 2.0, 1.99E-3 / 2.0, 0.16E-3 / 2.0], [0.01E-3/2.0, 0.010E-3/2.0, 0.01E-3/2.0, 0.01E-3/2.0]]).T

adf = np.array([1, 0.02]) # air density factor

d = np.array([3.6E-2, 0.3E-2]) # radius van cylinder


def V0_emp(r0, d, adf=1):
    _E0 = (33.7*adf + 0.813 * np.sqrt(adf/r0)) * 1.0E5
    _V0 = _E0 * r0 * np.log(d/r0)
    return [_E0, _V0]

def getV0E0err(r0_, adf_, d_, n=100000):

   # print("d", r0*2000)
    print("r0", r0_)

    r0 = sp.random.normal(r0_[0], r0_[1], n)  
    adf = sp.random.normal(adf_[0], adf_[1], n)
    d = sp.random.normal(d_[0], d_[1], n)

    # Whitehead’s empirical formula, error unknown
    E0 = (33.7*adf + 0.813 * np.sqrt(adf/r0)) * 1E5    

    E0_out = np.array([sp.mean(E0), sp.std(E0)])

    E0e, V0e = V0_emp(r0_[0], d_[0])

    V0 = E0 * r0 * np.log(d/r0)

    E0_out = np.array([E0e, sp.std(E0)])
    V0_out = np.array([V0e, sp.std(V0)])

    print("E0", E0_out, practicum.errornumtostr(E0_out))
    print("V0", V0_out, practicum.errornumtostr(V0_out))

    return [E0, V0]


for radius in r0:
    getV0E0err(radius, adf, d)
# %%

# %%
