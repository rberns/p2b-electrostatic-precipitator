# %%
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import glob

import seaborn as sns

import P2B
from practicum import *
import re

sns.set_theme()

size = "talk"

sns.set_context(size, font_scale=1.0)

palette_name = "mako"

sns.set_palette(palette_name)


#import style

# %%
# Nice styles


from cycler import cycler
import constanten as sc # sensor constants

#plt.style.use("dark_background")

#mpl.rcParams['axes.prop_cycle'] = cycler('color', ['#5ac8fa', '#007aff', '#4cd964', '#ff3b30', '#ffcc00', '#ff9500', '#e377c2', '#5856d6', '#ff2d55'])
#plt.rcParams['figure.figsize'] = [8.0, 8.0]


#plt.rcParams['figure.dpi'] = 300

PM10ERROR = 0.1
PM25ERROR = 0.1

# End of nice styles

# %%

def find_switch_times(df):
    # Where does the data change
    df['IsVandegraaffOnChange'] = df['IsVandegraaffOn'].diff()  

    # Time points
    switchtimepoints = np.append(df.loc[(df['IsVandegraaffOnChange'] != 0)]["Elapsed"].values, df["Elapsed"].values[-1])

    # Create iterator
    it = np.nditer(switchtimepoints, flags=['f_index'])

    ranges = []

    # sensor time delay
    sensor_time_delay = 180

    for elapsed in it:
        start = elapsed + sensor_time_delay
        if it.index + 1 < len(switchtimepoints):
            end = switchtimepoints[it.index + 1]
            ranges.append([start, end])
    
    return ranges


def grouper(df, ranges):
    i = 0
    lst = list()
    
    for index, row in df.iterrows():
        if row['Elapsed'] >= ranges[i][0] and row['Elapsed'] <= ranges[i][1]:
            lst.append(i+1)
        elif row['Elapsed'] >= ranges[i][1]:
            i += 1
            lst.append(0)
        else:
            lst.append(0)
        
    return np.array(lst)


def efficiency(mid, left, right):
    div = 2*P2B.fvf_delen2(mid, P2B.fvf_optellen2(left, right))
    div.T[0] = 1 - div.T[0]
    #print(div)
    return div


def match_file_name(file_name):
    p = re.compile(r'data\/(\d{2})(\w{3})\#(\d{2})(\w)v(\w)')
    return p.findall(file_name)[0]


def calc_efficiency(mean, is_v2, is_v3):
    if is_v2:
        return np.array([efficiency(mean[2], mean[1], mean[3]), efficiency(mean[4], mean[3], mean[5]), efficiency(mean[6], mean[5], mean[7])])
    elif is_v3:
        return np.array([efficiency(mean[2], mean[1], mean[3]), efficiency(mean[4], mean[3], mean[5])])
    else:
        return np.array([efficiency(mean[3], mean[2], mean[4]), efficiency(mean[5], mean[4], mean[6])])



def all_experiment_data(grouped_data, is_v2, is_v3):
    if not is_v3 or not is_v2:
        return grouped_data.loc[(grouped_data["VandeGraaffGroups"] != 1) & (grouped_data["VandeGraaffGroups"] != 0)]
    else:
        return grouped_data.loc[(grouped_data["VandeGraaffGroups"] != 0)]


def mean_error(series, min_error=None):
    if min_error is not None:
        error = np.maximum(series.std()/series.count(), min_error)
    else:
        error = series.std()/series.count()
    return np.array([series.mean(), error]).T

def filter_Van_de_Graaff(data, is_v2, is_v3, is_HIGH):
    if not is_v3 or not is_v2:
        if not is_HIGH:
            return data.loc[(data["VandeGraaffGroups"] != 1) & (data["VandeGraaffGroups"] != 0) & (data["IsVandegraaffOn"] == True)]
        else:
            return data.loc[(data["VandeGraaffGroups"] != 1) & (data["VandeGraaffGroups"] != 0) & (data["IsVandegraaffOn"] == False)]
    else:
        if not is_HIGH:
            return data.loc[(data["VandeGraaffGroups"] != 0) & (data["IsVandegraaffOn"] == True)]
        else:
            return data.loc[(data["VandeGraaffGroups"] != 0) & (data["IsVandegraaffOn"] == False)]



def analyser(file_name):
    # Data formatting version, dependent on the number ON/OFF intervals of the Van de Graaff generator
    is_v2 = False
    is_v3 = False


 

    # Set the figure size
    # plt.figure(figsize=(20,10), dpi=300)
    
    

    # Extracting experiment conditions from filename
    metadata = match_file_name(file_name)

    # Printing metadata
    print("day:", metadata[0], "month:", metadata[1], "EXP:", metadata[2], "ID:", metadata[3], "v:", metadata[4])

    setup_id = metadata[3]
    exp_id = "#{}-{}-{}".format(metadata[2], metadata[0], metadata[1])

    #plt.title("exp: {}, id: {}, v: {}".format(metadata[2], metadata[3], metadata[4]))
    
    # Set data_formatting
    if metadata[4] == '2':
        is_v2 = True
    elif metadata[4] == '3':
        is_v3 = True
    
    # Loading the file
    data = pd.read_pickle(file_name)

    # Removing errors from the data
    data = data.loc[(data["PM25"] != -1.0) & (data["PM10"] != -1.0) & (data["voltage"] >= 0)]



    # Get ranges
    ranges = find_switch_times(data)

    #print(data)

    data["VandeGraaffGroups"] = grouper(data, ranges)


    grouped = data.groupby("VandeGraaffGroups")

    meanPM10 = mean_error(grouped['PM10'], min_error=PM10ERROR)#np.array([grouped['PM10'].mean(), grouped['PM10'].std()/grouped['PM10'].count()]).T
    meanPM25 = mean_error(grouped['PM25'], min_error=PM25ERROR)#np.array([grouped['PM25'].mean(), grouped['PM25'].std()/grouped['PM25'].count()]).T


    used_data = all_experiment_data(data, is_v2, is_v3)

    # print(P2B.fvf_optellen2(mean[2], mean[4]))

    #res = np.array([efficiency(mean[3], mean[2], mean[4]), efficiency(mean[5], mean[4], mean[6])])

    res10 = calc_efficiency(meanPM10, is_v2, is_v3)
    res25 = calc_efficiency(meanPM25, is_v2, is_v3)


    gewogen10 = gewogen_gemiddelde2(res10)
    gewogen25 = gewogen_gemiddelde2(res25)

    #Temp = mean_error(used_data["Temperature"])
    MeanTemp = used_data["Temperature"].mean()
    Temp = np.array([MeanTemp, sc.errorvstemp(MeanTemp)])

    MeanHumidity = used_data["Humidity"].mean()
    Humidity = np.array([MeanHumidity, sc.errorvshumidity(MeanHumidity)])

    # Error from: https://cdn-shop.adafruit.com/datasheets/BST-BMP280-DS001-11.pdf
    MeanPressure = used_data["Pressure"].mean()
    Pressure = np.array([MeanPressure, 100])

    # All Van de Graaff ON
    high_values = filter_Van_de_Graaff(data, is_v2, is_v3, is_HIGH=True)
    low_values = filter_Van_de_Graaff(data, is_v2, is_v3, is_HIGH=False)

    #print(high_values)

    mean_high_PM10 = mean_error(high_values["PM10"], min_error=PM10ERROR)
    mean_high_PM25 = mean_error(high_values["PM25"], min_error=PM25ERROR)
    #print(mean_high_PM10)

    mean_low_PM10 = mean_error(low_values["PM10"], min_error=PM10ERROR)
    mean_low_PM25 = mean_error(low_values["PM25"], min_error=PM25ERROR)
    #print(mean_low_PM10)


    print("PM10", errornumtostr(gewogen10), "PM25", errornumtostr(gewogen25), "mean T", errornumtostr(Temp), "mean RH", errornumtostr(Humidity), "mean p", errornumtostr(Pressure), "N", used_data.shape[0])
    print("mean_high_PM10: {}, mean_low_PM10: {}, mean_high_PM25: {}, mean_low_PM25: {}".format(errornumtostr(mean_high_PM10), errornumtostr(mean_low_PM10), errornumtostr(mean_high_PM25), errornumtostr(mean_low_PM25)))

    elapsed_time = used_data.tail(1)['Elapsed'].values - used_data.head(1)['Elapsed'].values

    print("elapsed_time", elapsed_time)

    table_data = pd.DataFrame([[elapsed_time[0],    used_data.shape[0], Temp[0],            Temp[1],        Humidity[0],    Humidity[1], Pressure[0],   Pressure[1],    mean_high_PM25[0],  mean_high_PM25[1],  mean_low_PM25[0],   mean_low_PM25[1],   mean_high_PM10[0],  mean_high_PM10[1],  mean_low_PM10[0],   mean_low_PM10[1],   gewogen25[0],       gewogen25[1],       gewogen10[0],       gewogen10[1],       setup_id,       exp_id]], 
    columns=[                   "elapsed time",     "N",                "mean temperature", "stemperature", "humidity",     "shumidity", "pressure",    "spressure",    "PM25 HIGH",        "SPM25 HIGH",       "PM25 LOW",         "SPM25 LOW",        "PM10 HIGH",        "SPM10 HIGH",       "PM10 LOW",         "SPM10 LOW",        "efficiency PM25",  "sefficiency PM25", "efficiency PM10",  "sefficiency PM10", "setup id",     "experiment id"])

    print(table_data)

    plt.figure(figsize=(11.7/2, 8.27/2))
    plt.tight_layout()

    plt.plot(data["Elapsed"], data["PM25"], label="PM25")
    plt.plot(data["Elapsed"], data["PM10"], label="PM10")

    # for i, [start, end] in enumerate(ranges):
    #     isOn = grouped.get_group(i)["IsVandegraaffOn"]
    #     print(i, isOn, start, end)
    #     #print([start, end])
    #     #plt.axvline(start), color='#e377c2')
    #     #plt.axvline(end)#, color='#007aff')
    #     if isOn:
    #         plt.axvspan(start, end, color='green', alpha=0.5)
    #     else:
    #         plt.axvspan(start, end, color='grey', alpha=0.5)

    for group in grouped.groups.keys():
        if is_v2:
            if ((group < 1) or (group > 7)):
                continue
        elif is_v3:
            if ((group < 1) or (group > 5)):
                continue
        else:
            if ((group < 2) or (group > 6)):
                continue
        
        i_data = grouped.get_group(group)
        start = i_data["Elapsed"].values[0]
        end = i_data["Elapsed"].values[-1]

        isOn = i_data["IsVandegraaffOn"].values[0]
        #end = i_data["Elapsed"].values[-1]
        #print([start, end])
        #plt.axvline(start), color='#e377c2')
        #plt.axvline(end)#, color='#007aff')
        if isOn:
            plt.axvspan(start, end, color='#9ADFC0', alpha=0.5)
        else:
            plt.axvspan(start, end, color='grey', alpha=0.5)



    #plt.axvline()
    #plt.plot(data["Elapsed"], (data["voltage"]-data["voltage"].mean())*10000, color='#ffcc00')
    #plt.plot(data["Elapsed"], data["IsVandegraaffOn"])#, color='#ff9500')
    plt.xlabel("Elapsed (s)")
    plt.ylabel("Concentration ($\mu$g/cm$^3$)")
    plt.legend()

    plt.savefig("media/{}-concentration-plot-{}.pdf".format(exp_id, size))
    plt.figure(figsize=(11.7/2, 8.27/2))
    plt.tight_layout()
    
    for group in grouped.groups.keys():
        if is_v2:
            if ((group < 1) or (group > 7)):
                continue
        elif is_v3:
            if ((group < 1) or (group > 5)):
                continue
        else:
            if ((group < 2) or (group > 6)):
                continue
        
        i_data = grouped.get_group(group)
        start = i_data["Elapsed"].values[0]
        end = i_data["Elapsed"].values[-1]

        isOn = i_data["IsVandegraaffOn"].values[0]
        #end = i_data["Elapsed"].values[-1]
        #print([start, end])
        #plt.axvline(start), color='#e377c2')
        #plt.axvline(end)#, color='#007aff')
        if isOn:
            plt.axvspan(start, end, color='#9ADFC0', alpha=0.5)
        else:
            plt.axvspan(start, end, color='grey', alpha=0.5)

    #plt.figure()
    #plt.plot(data["Elapsed"], data["voltage"], label="voltage")
    plt.figure(figsize=(11.7/2, 8.27/2))

    plt.plot(data["Elapsed"], data["voltage"], label="voltage")

    

    plt.xlabel("Elapsed (s)")
    plt.ylabel("Sensor voltage (V)")
    for group in grouped.groups.keys():
        if is_v2:
            if ((group < 1) or (group > 7)):
                continue
        elif is_v3:
            if ((group < 1) or (group > 5)):
                continue
        else:
            if ((group < 2) or (group > 6)):
                continue
        
        i_data = grouped.get_group(group)
        start = i_data["Elapsed"].values[0]
        end = i_data["Elapsed"].values[-1]

        isOn = i_data["IsVandegraaffOn"].values[0]
        #end = i_data["Elapsed"].values[-1]
        #print([start, end])
        #plt.axvline(start), color='#e377c2')
        #plt.axvline(end)#, color='#007aff')
        if isOn:
            plt.axvspan(start, end, color='#9ADFC0', alpha=0.5)
        else:
            plt.axvspan(start, end, color='grey', alpha=0.5)

    plt.savefig("media/{}-ozonevoltage-plot-{}.pdf".format(exp_id, size))
    if is_v2 or is_v3:
        plt.figure(figsize=(11.7/2, 8.27/2))

        plt.plot(data["Elapsed"], data["voltage2"], label="voltage2")

        

        plt.xlabel("Elapsed (s)")
        plt.ylabel("Sensor voltage (V)")
        for group in grouped.groups.keys():
            if is_v2:
                if ((group < 1) or (group > 7)):
                    continue
            elif is_v3:
                if ((group < 1) or (group > 5)):
                    continue
            else:
                if ((group < 2) or (group > 6)):
                    continue
            
            i_data = grouped.get_group(group)
            start = i_data["Elapsed"].values[0]
            end = i_data["Elapsed"].values[-1]

            isOn = i_data["IsVandegraaffOn"].values[0]
            #end = i_data["Elapsed"].values[-1]
            #print([start, end])
            #plt.axvline(start), color='#e377c2')
            #plt.axvline(end)#, color='#007aff')
            if isOn:
                plt.axvspan(start, end, color='#9ADFC0', alpha=0.5)
            else:
                plt.axvspan(start, end, color='grey', alpha=0.5)

        plt.savefig("media/{}-ozonevoltage2-plot-{}.pdf".format(exp_id, size))

    
    return table_data
    


# %%

# Data output format
df = pd.DataFrame([], columns=["elapsed time",     "N",                "mean temperature", "stemperature", "humidity",     "shumidity", "pressure",    "spressure",    "PM25 HIGH",        "SPM25 HIGH",       "PM25 LOW",         "SPM25 LOW",        "PM10 HIGH",        "SPM10 HIGH",       "PM10 LOW",         "SPM10 LOW",        "efficiency PM25",  "sefficiency PM25", "efficiency PM10",  "sefficiency PM10", "setup id",     "experiment id"])

file_urls = glob.glob('data/*.pkl')

print(file_urls)



for url in file_urls:
    df = df.append(analyser(url), ignore_index=True)#)

print(df)

df.to_csv("table_out.pkl")



plt.show()

# %%

# %%

# %%
