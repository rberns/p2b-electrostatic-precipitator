# %% 
# Stroom geproduceerd door de Van de Graaf generator.

import practicum
import P2B

V = np.array([1840, 70])
R = np.array([1000, 100])

I = P2B.fvf_delen2(V, R)

print(practicum.errornumtostr(I), I)
# %%
