# %%
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import matplotlib.ticker as mtick
from matplotlib import colors
import json 
import os
import style


plt.style.use('seaborn')
import numpy as np

import P2B

#sns.set_context("paper", font_scale=1.0)
sns.set_context("talk", font_scale=1.0)

data = pd.read_csv("table_out.pkl")
data = data.sort_values(['setup id'])

print(data)

# %%

grouped_data = data.groupby("setup id")

for group in grouped_data.groups.keys():
   print(grouped_data.get_group(group))

   

#print(result)

#colors = ['#5ac8fa', '#007aff', '#4cd964', '#ff3b30', '#ffcc00', '#ff9500', '#e377c2', '#5856d6', '#ff2d55']

# %%

def add_color(row):
    if row['setup id'] == 'a':
       return style.color[0]
    if row['setup id'] == 'b':
       return style.color[1]
    if row['setup id'] == 'c':
       return style.color[2]
    if row['setup id'] == 'd':
       return style.color[3]
    if row['setup id'] == 'e':
       return style.color[4]
    return style.color[5]

data['color'] = data.apply(lambda row: add_color(row), axis=1)
data.reset_index()

data.plot(kind = "barh", y = "efficiency PM25", legend = False, title = "$\eta \\ PM25$", xerr = "sefficiency PM25", x="setup id", color=data['color'])
data.plot(kind = "barh", y = "efficiency PM10", legend = False, title = "$\eta \\ PM10$", xerr = "sefficiency PM10", x="setup id", color=data['color'])

plt.show()




# %%

# %%
