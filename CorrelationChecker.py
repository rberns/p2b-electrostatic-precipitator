#%%

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import glob
import style


import P2B
from practicum import *
import re
import seaborn as sns
# Nice styles

from cycler import cycler
import constanten as sc 

# %%

data = pd.read_csv("table_out.pkl")
data = data.sort_values(['setup id'])

# %%

data.corr(method ='pearson')
# %%
