import practicum
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def fvf_delen(x, Sx, y, Sy):
    term1 = ((1/y)**2)*Sx**2
    term2 = ((x/(y**2))*Sy)**2
    Sr = np.sqrt(term1+term2)
    return [x/y, Sr]


def fvf_delen2(x, y):
    return np.array(fvf_delen(x[0], x[1], y[0], y[1]))


def fvf_vermeningvuldiging(x, Sx, y, Sy):
    return [x*y, np.sqrt((y*Sx)**2+(x*Sy)**2)]


def fvf_verschil2(x, y):
    return np.array(fvf_verschil(x[0], x[1], y[0], y[1]))


def fvf_optellen2(x, y):
    return np.array(fvf_optellen(x[0], x[1], y[0], y[1]))


def fvf_optellen(x, Sx, y, Sy):
    return [x+y, np.sqrt(Sx**2+Sy**2)]


def fvf_verschil(x, Sx, y, Sy):
    return fvf_optellen(x, Sx, -y, Sy)


def std(list):
    # standaard deviatie volgens de practicum standaard
    return np.std(list, ddof=1)
