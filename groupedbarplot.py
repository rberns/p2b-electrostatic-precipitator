# %%
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from practicum import *

import matplotlib.ticker as mtick
from matplotlib import colors
import json 
import os
import style


plt.style.use('seaborn')
import numpy as np

import P2B

#sns.set_context("paper", font_scale=1.0)
sns.set_context("talk", font_scale=1.0)

data = pd.read_csv("table_out.pkl")
data = data.sort_values(['setup id'])

#a4_dims = (11.7, 8.27)
plt.figure(figsize=(11.7/2, 8.27/2))
plt.tight_layout()

print(data)

# %%

grouped_data = data.groupby("setup id")

gewogeneff25 = []
gewogeneff10 = []
setup_id = []

for group in grouped_data.groups.keys():
   internal_data = grouped_data.get_group(group)

   gewogeneff25_ = gewogen_gemiddelde2(np.array([internal_data["efficiency PM25"].values, internal_data["sefficiency PM25"].values]).T)
   gewogeneff10_ = gewogen_gemiddelde2(np.array([internal_data["efficiency PM10"].values, internal_data["sefficiency PM10"].values]).T)

   gewogeneff25.append(gewogeneff25_)
   gewogeneff10.append(gewogeneff10_)
   
   setup_id.append(group)

gewogeneff25 = np.array(gewogeneff25).T
gewogeneff10 = np.array(gewogeneff10).T

print(gewogeneff25)
print(gewogeneff10)

pm2510 = P2B.fvf_delen2(gewogeneff25, gewogeneff10)

print("pm2510", pm2510)

#print(result)
d = {"$\eta \\ (\\%) \\ PM10$": gewogeneff10[0]*100, "SPM10": gewogeneff10[1]*100, "$\eta \\ (\\%) \\ PM25$": gewogeneff25[0]*100, "SPM25": gewogeneff25[1]*100, "PM25:PM10": pm2510[0], "SPM25:PM10": pm2510[1], "Setup": setup_id}
df = pd.DataFrame(data=d)

print(df)

#colors = ['#5ac8fa', '#007aff', '#4cd964', '#ff3b30', '#ffcc00', '#ff9500', '#e377c2', '#5856d6', '#ff2d55']

# %%

def add_color(row, name="setup id"):
    if row[name] == 'a':
       return style.color[0]
    if row[name] == 'b':
       return style.color[1]
    if row[name] == 'c':
       return style.color[2]
    if row[name] == 'd':
       return style.color[3]
    if row[name] == 'e':
       return style.color[4]
    return style.color[5]

data['color'] = data.apply(lambda row: add_color(row), axis=1)
data.reset_index()


df['color'] = df.apply(lambda row: add_color(row, name="Setup"), axis=1)
df.plot(kind = "barh", y = "$\eta \\ (\\%) \\ PM25$", legend = False, title = "$\eta \\ (\\%) \\ PM2.5$", xerr = "SPM10", x="Setup", color=df['color'], xlim=[0, 100])

plt.savefig("media/efficiencyPM25.pdf")

plt.figure(figsize=(11.7/2, 8.27/2))
plt.tight_layout()
df.plot(kind = "barh", y = "$\eta \\ (\\%) \\ PM10$", legend = False, title = "$\eta \\ (\\%) \\ PM10$", xerr = "SPM25", x="Setup", color=df['color'], xlim=[0, 100])

plt.savefig("media/efficiencyPM10.pdf")

data.plot(kind = "barh", y = "efficiency PM25", legend = False, title = "$\eta \\ PM25$", xerr = "sefficiency PM25", x="setup id", color=data['color'])
data.plot(kind = "barh", y = "efficiency PM10", legend = False, title = "$\eta \\ PM10$", xerr = "sefficiency PM10", x="setup id", color=data['color'])

plt.figure(figsize=(11.7/2, 8.27/2))
plt.tight_layout()
df.plot(kind = "barh", y = "PM25:PM10", legend = False, title = "$\\frac{\eta_{PM25}}{\eta_{PM10}}$", xerr = "SPM25:PM10", x="Setup", color=df['color'], xlim=[0, 1])
plt.savefig("media/ratioPM25PM10.pdf")

plt.show()




# %%

# %%
