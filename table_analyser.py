# %%
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import glob
import style


import P2B
from practicum import *
import re
import seaborn as sns

import background_gradient_func
# Nice styles

from cycler import cycler
import constanten as sc 




th_props = [
  ('font-size', '11px'),
  ('text-align', 'center'),
  ('font-weight', 'regular'),
  ('color', '#000'),
  ('background-color', '#f7f7f9'),
  ('border', 'thin solid black'),
  ('font-family', "'Open Sans', sans-serif")
  ]

# Set CSS properties for td elements in dataframe
td_props = [
  ('font-size', '11px'),
  ('font-family', "'Open Sans', sans-serif"),
  ('border', 'thin solid black')
  #,
  #('border', '1px solid #dddddd')
  ]

# Set table styles
styles = [
  dict(selector="th", props=th_props),
  dict(selector="td", props=td_props),
  #dict(selector="thead", props = [('display', 'none')]),
  dict(
        props=[
            ('border-collapse', 'collapse')
        ]
    ),
]


def highlight_max(s):
    '''
    highlight the maximum in a Series yellow.
    '''
    is_max = s == s.max()
    return ['background-color: yellow' if v else '' for v in is_max]

# %%

df = pd.read_csv("table_out.pkl")

df = df.sort_values(['setup id'])

print(df)

df["$Setup$"] = df["setup id"]

df["$\eta \\ (\\%) \\ PM2.5$"] = formatted_errornumstostrs(np.array([df["efficiency PM25"].values*100, df["sefficiency PM25"].values*100]).T)
df["$\eta \\ (\\%) \\ PM10$"] = formatted_errornumstostrs(np.array([df["efficiency PM10"].values*100, df["sefficiency PM10"].values*100]).T)

df["$PM2.5 \\ HIGH \\ (\mu g/m^3)$"] = formatted_errornumstostrs(np.array([df["PM25 HIGH"].values, df["SPM25 HIGH"].values]).T, formatting="${:.2f} \pm {:.2f}$")
df["$PM2.5 \\ LOW \\ (\mu g/m^3)$"] = formatted_errornumstostrs(np.array([df["PM25 LOW"].values, df["SPM25 LOW"].values]).T, formatting="${:.2f} \pm {:.2f}$")

df["$PM10 \\ HIGH \\ (\mu g/m^3)$"] = formatted_errornumstostrs(np.array([df["PM10 HIGH"].values, df["SPM10 HIGH"].values]).T, formatting="${:.2f} \pm {:.2f}$")
df["$PM10 \\ LOW \\ (\mu g/m^3)$"] = formatted_errornumstostrs(np.array([df["PM10 LOW"].values, df["SPM10 LOW"].values]).T, formatting="${:.2f} \pm {:.2f}$")



# html = (df[['', '']].style.bar(subset=['PM25 HIGH', 'PM10 HIGH'], color='#e377c2')
#             .bar(subset=['PM25 LOW', 'PM10 LOW'], color='#5856d6')
#             .apply(background_gradient_func._background_gradient, subset=["$\eta \\ (\\%) PM2.5$"], cmap="mako", gmap=df["efficiency PM25"])
#             .apply(background_gradient_func._background_gradient, subset=["$\eta \\ (\\%) PM10$"], cmap="mako", gmap=df["efficiency PM10"])
#             .set_table_styles(styles)
#             .render())

#df.set_index('experiment id', inplace=True)
df.reset_index(drop=True, inplace=True)

html = (df[["$Setup$", "$\eta \\ (\\%) \\ PM2.5$", "$\eta \\ (\\%) \\ PM10$", "$PM2.5 \\ HIGH \\ (\mu g/m^3)$", "$PM10 \\ HIGH \\ (\mu g/m^3)$", "$PM2.5 \\ LOW \\ (\mu g/m^3)$", "$PM10 \\ LOW \\ (\mu g/m^3)$"]].style.hide_index()
            .apply(background_gradient_func._background_gradient, subset=["$\eta \\ (\\%) \\ PM2.5$"], cmap=style.cmaplight, gmap=df["efficiency PM25"])
            .apply(background_gradient_func._background_gradient, subset=["$\eta \\ (\\%) \\ PM10$"], cmap=style.cmaplight, gmap=df["efficiency PM10"])
            .apply(background_gradient_func._background_gradient, subset=["$PM2.5 \\ HIGH \\ (\mu g/m^3)$"], cmap=style.cmaplight, gmap=df["PM25 HIGH"])
            .apply(background_gradient_func._background_gradient, subset=["$PM2.5 \\ LOW \\ (\mu g/m^3)$"], cmap=style.cmaplight, gmap=df["PM25 LOW"])
            .apply(background_gradient_func._background_gradient, subset=["$PM10 \\ HIGH \\ (\mu g/m^3)$"], cmap=style.cmaplight, gmap=df["PM10 HIGH"])
            .apply(background_gradient_func._background_gradient, subset=["$PM10 \\ LOW \\ (\mu g/m^3)$"], cmap=style.cmaplight, gmap=df["PM10 LOW"])
            .set_table_styles(styles)
            .render())

pre = """<script>
  MathJax = {
    tex: {inlineMath: [['$', '$'], ['\\(', '\\)']]}
  };
  </script>
  <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js"></script>"""

table_file_out = open("table.html", "w")
table_file_out.write(pre + html)
table_file_out.close()
# %%


# Have a look at: https://seaborn.pydata.org/generated/seaborn.kdeplot.html fill
# %%

# %%
