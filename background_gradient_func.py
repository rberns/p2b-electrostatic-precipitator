from __future__ import annotations
from pandas import *

from contextlib import contextmanager
import copy
from functools import partial
import operator
from typing import (
    Any,
    Callable,
    Hashable,
    Sequence,
)
import warnings

import numpy as np

from pandas._typing import (
    Axis,
    FrameOrSeries,
    FrameOrSeriesUnion,
    IndexLabel,
    Scalar,
)
from pandas.compat._optional import import_optional_dependency
from pandas.util._decorators import doc

import pandas as pd
from pandas.api.types import is_list_like
from pandas.core import generic
import pandas.core.common as com
from pandas.core.frame import (
    DataFrame,
    Series,
)
from pandas.core.generic import NDFrame

jinja2 = import_optional_dependency("jinja2", extra="DataFrame.style requires jinja2.")

# from pandas.io.formats.style_render import (
#     CSSProperties,
#     CSSStyles,
#     StylerRenderer,
#     Subset,
#     Tooltips,
#     maybe_convert_css_to_tuples,
#     non_reducing_slice,
# )

try:
    from matplotlib import colors
    import matplotlib.pyplot as plt

    has_mpl = True
except ImportError:
    has_mpl = False
    no_mpl_message = "{0} requires matplotlib."


@contextmanager
def _mpl(func: Callable):
    if has_mpl:
        yield plt, colors
    else:
        raise ImportError(no_mpl_message.format(func.__name__))

# Sequence | np.ndarray | FrameOrSeries | None = None
def _background_gradient(
    data,
    cmap="PuBu",
    low: float = 0,
    high: float = 0,
    text_color_threshold: float = 0.408,
    vmin = None,
    vmax = None,
    gmap = None,
):
    """
    Color background in a range according to the data or a gradient map
    """
    #gmap = data.to_numpy(dtype=float)
    #if gmap is None:  # the data is used the gmap
    #    gmap = data.to_numpy(dtype=float)
    #else:  # else validate gmap against the underlying data
    print(gmap)

    gmap = gmap.to_numpy(dtype=float)

    with _mpl(io.formats.style.Styler.background_gradient) as (plt, colors):
        smin = np.nanmin(gmap) if vmin is None else vmin
        smax = np.nanmax(gmap) if vmax is None else vmax
        rng = smax - smin
        # extend lower / upper bounds, compresses color range
        norm = colors.Normalize(smin - (rng * low), smax + (rng * high))
        rgbas = plt.cm.get_cmap(cmap)(norm(gmap))

        def relative_luminance(rgba) -> float:
            """
            Calculate relative luminance of a color.
            The calculation adheres to the W3C standards
            (https://www.w3.org/WAI/GL/wiki/Relative_luminance)
            Parameters
            ----------
            color : rgb or rgba tuple
            Returns
            -------
            float
                The relative luminance as a value from 0 to 1
            """
            r, g, b = (
                x / 12.92 if x <= 0.04045 else ((x + 0.055) / 1.055) ** 2.4
                for x in rgba[:3]
            )
            return 0.2126 * r + 0.7152 * g + 0.0722 * b

        def css(rgba) -> str:
            dark = relative_luminance(rgba) < text_color_threshold
            text_color = "#f1f1f1" if dark else "#000000"
            return f"background-color: {colors.rgb2hex(rgba)};color: {text_color};"

        if data.ndim == 1:
            return [css(rgba) for rgba in rgbas]
        else:
            return DataFrame(
                [[css(rgba) for rgba in row] for row in rgbas],
                index=data.index,
                columns=data.columns,
            )
