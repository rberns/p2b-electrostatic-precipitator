# %%

import numpy as np
import scipy.constants as constants
import matplotlib.pyplot as plt

import seaborn as sns

plt.style.use('seaborn')

sns.set_context("talk", font_scale=1.0)
import style
sns.set_palette(sns.color_palette(style.palette_name))

figsize = (11.7/2, 8.27/2)
#figsize = (11.7, 8.27)

V = np.array([7E3, 12.5E3, 20E3])
SV = np.array([[1E3, 2E3, 2E3],[1E3, 2E3, 20E3]])

plt.figure(figsize=figsize)

P = 101325.0 #760/760 #101325.0 / 101325.0 * (748/760) # p/p0 (p0 is atmospheric pressure)

T = 293 # in Celsius

r0 = np.array([0.105E-3 / 2.0, 0.5195E-3 / 2.0, 1.99E-3 / 2.0])
sr0 = np.array([0.01E-3/2, 0.01E-3/2, 0.01E-3/2])

d = 3.6E-2 #3.6E-2

L = 12.7E-2 # 12.7 cm

adf = 1
print("d", r0*2000)
print("r0", r0)
# Whitehead’s empirical formula
E0 = (33.7*adf + 0.813 * np.sqrt(adf/r0)) * 1E5

print("E0", E0)

V0_ = E0 * r0 * np.log(d/r0)

print("V0_" ,V0_)

def V0_emp(r0, d, adf=1):
    _E0 = (33.7*adf + 0.813 * np.sqrt(adf/r0)) * 1E5
    _V0 = _E0 * r0 * np.log(d/r0)
    return _V0

r = np.linspace(1E-6, 1E-3, 100)

plt.errorbar(x=r0*1000, xerr=sr0*1000, y=V, yerr=SV, label="Experimental voltage", fmt='o')
plt.errorbar(x=r*1000, y=V0_emp(r, d), label="Corona onset voltage")
plt.xlabel("$r_0$ (mm)")
plt.ylabel("$V$ (V)")
plt.legend()
plt.tight_layout()
plt.ylim(0, 25000)
plt.savefig("media/Coronaonset.pdf")
plt.show()


# %%

# %%
