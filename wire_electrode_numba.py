import numpy as np
import matplotlib.pyplot as plt
#from numba import jit


from numerical_methods_numba import *



width, height = 1, 1
n_width, n_height  = 1000, 1000
prism = np.zeros((n_width, n_height), dtype=float)
boundary_location = np.zeros((n_width, n_height), dtype=int)


print(prism)

m = (int(n_width/2), int(n_height/2))
r0 = 20
r = 500

for i in range(int(n_width)):
	for j in range(int(n_height)):
		length = ((i-m[0])**2 + (j-m[1])**2)
		if length < r0**2:
			prism[j, i] = 2500
			boundary_location[j, i] = 1
			#boundary_location.append((j,i))
		if length > r**2:
			prism[j, i] = 0
			boundary_location[j, i] = 1
			#boundary_location.append((j,i))


print(prism)

x = np.linspace(-width/2, width/2, num=n_width)
y = np.linspace(-height/2, height/2, num=n_height)

h = plt.contourf(x, y, prism)

plt.show()

# boundary_location = []
# for i in range(int(width * 0.4), int(width * 0.6)):
# 	for j in range(int(width * 0.4), int(width * 0.6)): 
# 		prism[i][j] = 1
# 		boundary_location.append((i,j))



# output_matrix = two_dimension_jacobi(prism, boundary_location, deciVolt, middle_matrix=25)
# output_matrix = two_dimension_gauss_seidel(prism, boundary_location, .0001)
# output_matrix = two_dimension_gauss_seidel(prism, boundary_location, 1)
# output_matrix = two_dimension_sor_alpha(prism, boundary_location, 1.5, 1000)

output_matrix = two_dimension_gauss_seidel(prism, boundary_location, 100)

plt.imshow(output_matrix[0])
plt.show()

# fig, (ax1,ax2,ax3) = plt.subplots(1,3)
# first = ax1.imshow(prism, cmap="bone")
# first = ax2.imshow(output_matrix, cmap="bone")
# #first = ax3.imshow(output_matrix[0], cmap="bone")
# fig.colorbar(first, ax=[ax1, ax2], orientation="horizontal")

print("Finished!")

# Show Voltage
jacobi_solution = output_matrix[0]
coordinates  = np.linspace(0, 1.0, len(jacobi_solution))
X, Y   = np.meshgrid(x, y)
dx, dy = np.diff(X, axis=1).item(0), np.diff(Y, axis=0).item(0)
Ey, Ex = np.gradient(- jacobi_solution)
Z      = np.sqrt((Ex/dx) ** 2 + (Ey/dy) **2)
plt.imshow(Z, cmap="bone")

plt.show()

#np.savetxt("Z_prism.csv", Z, delimiter=",")
plt.streamplot(X, Y, Ex, Ey, density=1, color='k') #linewidth=Z/2)

plt.show()
