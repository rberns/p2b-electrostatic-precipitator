# %%
import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage
import matplotlib.patches as patches
#from numba import jit

#from numerical_methods_numba import *
import seaborn as sns

sns.set_context("paper", font_scale=2.0)

import cv2
import style



extent = (-0.05, 0.05, -0.05, 0.05)
figsize=(11.7, 8.27)

# %%

def process_img(image, result, width, height):
	img = np.ceil(image/255.0)
	
	for ix, iy, iz in np.ndindex(img.shape):
		if iz == 0:
			result[ix, iy] = img[ix, iy].max()

	return result



def image_to_matrix(path, width, height):
	image = cv2.imread(path)
	result = np.zeros((width, height), dtype=float)
	return process_img(image, result, width, height)


# %%

width, height = 0.1, 0.1
n_width, n_height  = 1000, 1000
x = np.linspace(-width/2, width/2, num=n_width)
y = np.linspace(-height/2, height/2, num=n_height)

prism = image_to_matrix("WireBrushDesignPotential.png", n_width, n_height).copy() * 5000
boundary_location = image_to_matrix("WireBrushDesignBoundary.png", n_width, n_height).copy()


with open('matrix.npy', 'rb') as f:
    output_matrix = np.load(f)

# %%
plt.figure(figsize=figsize)
plt.tight_layout()
plt.imshow(output_matrix, cmap="mako", extent=extent)
plt.xlabel("$x$ (m)")
plt.ylabel("$y$ (m)")
plt.colorbar(label="$V$ (V)")


ax = plt.gca()
rect = patches.Circle((0, 0), 0.036, linewidth=1, edgecolor='w', facecolor='none', linestyle='dashed')
ax.add_patch(rect)

plt.savefig("media/Theoreticalvoltage.pdf")
plt.show()

# fig, (ax1,ax2,ax3) = plt.subplots(1,3)
# first = ax1.imshow(prism, cmap="bone")
# first = ax2.imshow(output_matrix, cmap="bone")
# #first = ax3.imshow(output_matrix[0], cmap="bone")
# fig.colorbar(first, ax=[ax1, ax2], orientation="horizontal")

print("Finished!")

# %%
# np.arange(16)+0.05, np.arange(16)-0.05]
t = np.array([np.arange(16), np.arange(16)+0.2, np.arange(16)-0.2]).flatten()
print(t)

xp = 0.02*np.cos(t*2*np.pi/16)
yp = 0.02*np.sin(t*2*np.pi/16)

#print(xp, yp)

start_coords = np.array([xp, yp]).T
# %%



# Show Voltage
jacobi_solution = output_matrix
coordinates  = np.linspace(0, 1.0, len(jacobi_solution))
X, Y   = np.meshgrid(x, y)
dx, dy = np.diff(X, axis=1).item(0), np.diff(Y, axis=0).item(0)
Ey, Ex = np.gradient(- jacobi_solution)
Z      = np.sqrt((Ex/dx) ** 2 + (Ey/dy) **2)


plt.figure(figsize=figsize)
plt.tight_layout()
plt.imshow(Z, cmap="mako", extent=extent)
plt.xlabel("$x$ (m)")
plt.ylabel("$y$ (m)")
plt.colorbar(label="$E$ (N/C)")

ax = plt.gca()
rect = patches.Circle((0, 0), 0.036, linewidth=1, edgecolor='w', facecolor='none', linestyle='dashed')
ax.add_patch(rect)

plt.savefig("media/Theoreticalelectricfield.pdf")


plt.show()

# %%

#np.savetxt("Z_prism.csv", Z, delimiter=",")
plt.figure(figsize=figsize)
ax = plt.gca()
plt.tight_layout()
imshowres = ax.imshow(Z, cmap="mako", extent=extent)
plt.streamplot(X, Y, Ex, Ey, density=50, color='r', start_points=start_coords) #linewidth=Z/2)
plt.xlabel("$x$ (m)")
plt.ylabel("$y$ (m)")
plt.colorbar(imshowres, label="$E$ (N/C)")

rect = patches.Circle((0, 0), 0.036, linewidth=1, edgecolor='w', facecolor='none', linestyle='dashed')
ax.add_patch(rect)

plt.savefig("media/streamplot.pdf")



plt.show()

# %%



#ax.imshow(boundary_location, cmap="crest", extent=extent, interpolation="none")
plt.figure(figsize=figsize)
plt.tight_layout()
ax = plt.gca()
ax.imshow(Z, cmap="mako", extent=extent)

ax.contour(X, Y, Z, levels=7)
plt.xlabel("$x$ (m)")
plt.ylabel("$y$ (m)")
plt.colorbar(imshowres, label="$E$ (N/C)")
plt.xlim(-0.005, 0.005)
plt.ylim(-0.015, -0.005)


plt.show()
# %%

num = 1000
x, y = np.linspace(500, 500, num), np.linspace(0, 500, num)

yscaled = np.linspace(-0.05, 0, num)

print(Z)

#zi = scipy.ndimage.map_coordinates(Z, np.vstack((x,y)))
zi = output_matrix[x.astype(np.int), y.astype(np.int)]

print(zi)

plt.figure(figsize=np.array(figsize)/2)
plt.tight_layout()
plt.plot(yscaled, zi)

r0 = np.array([0.105E-3 / 2.0, 0.5195E-3 / 2.0, 1.99E-3 / 2.0])

d = np.array([3.6E-2]) # radius van cylinder

def V0_emp(r0_, d, adf=1):
    _E0 = (33.7*adf + 0.813 * np.sqrt(adf/r0_)) * 1.0E5
    _V0 = _E0 * r0_ * np.log(d/r0_)
    return [_E0, _V0]


def E_field_at(r, V0, d, r0):
	return ((V0/r) * np.log(d/r0))


def V_at(r, V0, d, r0):
	res = []
	for r_i in r:
		if r_i > r0:
			res.append(((V0/np.log(r0/d)))*np.log(r_i/d))
		else:
			res.append(V0)
	return np.array(res)


for radius in r0:
	E0, V0 = V0_emp(radius, d)
	#E = E_field_at(np.abs(yscaled),V0,d,radius)
	V = V_at(np.abs(yscaled),V0,d,radius)
	plt.plot(yscaled, V)

plt.show()


# %%
