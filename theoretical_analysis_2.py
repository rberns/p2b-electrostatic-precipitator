# Theoretical analysis 2, I-V characterisitc

# %%
import numpy as np
import scipy.constants as constants


# %%

P = 101325.0 #760/760 #101325.0 / 101325.0 * (748/760) # p/p0 (p0 is atmospheric pressure)

T = 293 # in Celsius

r0 = np.array([0.105E-3 / 2.0, 0.5195E-3 / 2.0, 1.99E-3 / 2.0])

d = 3.6E-2 #3.6E-2

L = 12.7E-2 # 12.7 cm

# %%

#Tnorm = 293.0 / (273.0 + T) 

#print("Tnorm", Tnorm)

# air density factor = p[Torr] / 760 * 293/(273+T)

# air density factor δ
#adf = (P/Tnorm) #2.94E-3 * (P/T)

adf = 1
print("d", r0*2000)
print("r0", r0)
# Whitehead’s empirical formula
E0 = (33.7*adf + 0.813 * np.sqrt(adf/r0)) * 1E5

print("E0",E0)

V0_ = E0 * r0 * np.log(d/r0)

print("V0_" ,V0_)

# onset voltage of the positive dc corona discharge, from 1. Yehia, A., Abdel-Fattah, E. & Mizuno, A. Positive direct current corona discharges in single wire-duct electrostatic precipitators. AIP Adv. 6, 055213 (2016).
term1 = 9907.8 * (P/T)
term2 = np.sqrt(1.943E7*(P/(T*r0)))*r0*np.log(d/r0)

V0 = term1 + term2

print("V0", V0)
# %%

mu = 1.4E-4

V = np.array([7E3, 12.5E3, 25.0E3])

#I = 2 * np.pi * constants.epsilon_0 * mu * L * (V - V0_)**2 / d**2

I = 8 * np.pi * constants.epsilon_0 * mu * L * V * (V - V0_) / (d**2 * np.log(d/r0))



# Townsend, J. S. (1914). XI. The potentials required to maintain currents between coaxial cylinders . The London, Edinburgh, and Dublin Philosophical Magazine and Journal of Science, 28(163), 83–90. https://doi.org/10.1080/14786440708635186
#k = 1.8E-14
#I = k * L * (V - V0_) * V0_ / (d**2 * np.log(d/r0))

#I = 2 * np.pi * constants.epsilon_0 * mu * L * (V-V0_) * (V - V0_) / (d**2 * np.log(d/r0))

#k = 4.5 # m/s = 450 cm/s

#
#X = V0_#V / (r0*np.log(d/r0))

#E1 = V/(r0*np.log(d/r0))

#print("E1", E1)

#E2term1 = (1-(r0/d)**2)*I/(2*np.pi*constants.epsilon_0*mu*L)
#E2term2 = V0_/(r0*np.log(d/r0))

#E2 = 



print("I [uA]", I*1.0E6)

# %%

# %%
# Checkout M. P. Sarma and W. Janischewskyj, Proc. IEEE 116(1), 161 (1969)

# The great different method: 1. Yehia, A. & Mizuno, A. Ozone generation by negative direct current corona discharges in dry air fed coaxial wire-cylinder reactors. J. Appl. Phys. 113, 183301 (2013).
# But wait it's negative corona so it might be incorrect
# %%

# %%

# %%

